// These constants won't change:
const int analogPin = A0;    // pin that the sensor is attached to
const int ledPinM = 9;   
const int ledPinZ = 11;
const int ledPinR = 10;  // pin that the LED is attached to
const int threshold1 = 800;
const int threshold2 = 950;   // an arbitrary threshold level that's in the range of the analog input

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPinR, OUTPUT);
  pinMode(ledPinZ, OUTPUT);
  pinMode(ledPinM, OUTPUT);
  // initialize serial communications:
  Serial.begin(9600);
}

void loop() {
  // read the value of the potentiometer:
  int analogValue =analogRead(analogPin);

  // if the analog value is high enough, turn on the LED:
  if (analogValue < threshold1) {
    digitalWrite(ledPinM, LOW);
    digitalWrite(ledPinR, HIGH);
    digitalWrite(ledPinZ, HIGH);
    delay(1000);
  } 
  if (threshold1 < analogValue && analogValue < threshold2) {
    digitalWrite(ledPinZ, LOW);
    digitalWrite(ledPinM, HIGH);
    digitalWrite(ledPinR, HIGH);
    delay(1000);
  }
  if (analogValue > threshold2) {
    digitalWrite(ledPinR, LOW);
    digitalWrite(ledPinM, HIGH);
    digitalWrite(ledPinZ, HIGH);
    delay(1000);
  }

  // print the analog value:
  Serial.println(analogValue);
  delay(1);        // delay in between reads for stability
}
