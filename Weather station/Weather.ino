#include "DHT.h"

int redPin = 11;
int greenPin = 10;
int bluePin = 9;

int redPin1 = 6;
int greenPin1 = 3;
int bluePin1 = 5;

#define DHTPIN 2

DHT dht(DHTPIN, DHT22);


void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT); 
  Serial.begin(9600);
 
  dht.begin();
}


void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.println(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println(" °C ");
    // DHT11 sampling rate is 1HZ.
  
   if (h < 20){
    setColourRgb(0, 200, 200);}
  else if (h < 60){
    setColourRgb(200, 0, 200);}
  else {
    setColourRgb(200, 200, 0);}



 if (t > 25){
    setColourRgb1(0, 200, 200);}
  else if (t > 15){
    setColourRgb1(200, 0, 200);}
  else {
    setColourRgb1(200, 200, 0);}
    delay(1000);
    
}

void setColourRgb(unsigned int red, unsigned int green, unsigned int blue) {
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);
}                  

void setColourRgb1(unsigned int red, unsigned int green, unsigned int blue) {
  analogWrite(redPin1, red);
  analogWrite(greenPin1, green);
  analogWrite(bluePin1, blue);
}                  
                      
